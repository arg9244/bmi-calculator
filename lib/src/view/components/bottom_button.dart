import 'package:bmi_calculator/src/logic/models/consts.dart';
import 'package:flutter/material.dart';

class BottomButton extends StatelessWidget {
  final String text;
  final VoidCallback onPressed;

  BottomButton({
    @required this.onPressed,
    @required this.text,
  });

  @override
  Widget build(BuildContext context) {
    return RaisedButton(
      padding: EdgeInsets.all(15.0),
      color: Theme.of(context).accentColor,
      onPressed: onPressed,
      child: Text(
        text,
        style: kBottomButtonTextStyle,
      ),
    );
  }
}
