import 'package:bmi_calculator/src/logic/models/consts.dart';
import 'package:bmi_calculator/src/view/components/round_icon_button.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class NumberSelector extends StatelessWidget {
  const NumberSelector({
    @required this.name,
    @required this.number,
    @required this.onIncreament,
    @required this.onDecrement,
  });

  final String name;
  final int number;
  final VoidCallback onIncreament;
  final VoidCallback onDecrement;

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        Text(
          name,
          style: kCustomIconLabelTextStyle,
        ),
        Text(
          number.toString(),
          style: kCustomIconNumberTextStyle,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            RoundIconButton(
              onPressed: onDecrement,
              icon: FontAwesomeIcons.minus,
            ),
            SizedBox(
              width: 15.0,
            ),
            RoundIconButton(
              onPressed: onIncreament,
              icon: FontAwesomeIcons.plus,
            ),
          ],
        ),
      ],
    );
  }
}
