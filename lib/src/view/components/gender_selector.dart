import 'package:bmi_calculator/src/logic/models/consts.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class GenderIcon extends StatelessWidget {
  final Gender iconGender;
  GenderIcon({@required this.iconGender});

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Icon(
          iconGender == Gender.male
              ? FontAwesomeIcons.mars
              : FontAwesomeIcons.venus,
          size: 80.0,
        ),
        SizedBox(
          height: 15.0,
        ),
        Text(
          iconGender == Gender.male ? 'MALE' : 'FEMALE',
          style: kCustomIconLabelTextStyle,
        ),
      ],
    );
  }
}
