import 'package:bmi_calculator/src/logic/bmi_calculator.dart';
import 'package:bmi_calculator/src/logic/models/consts.dart';
import 'package:bmi_calculator/src/view/components/bottom_button.dart';
import 'package:bmi_calculator/src/view/components/custom_card.dart';
import 'package:bmi_calculator/src/view/components/gender_selector.dart';
import 'package:bmi_calculator/src/view/components/number_selector.dart';
import 'package:bmi_calculator/src/view/screens/result.dart';
import 'package:flutter/material.dart';

class InputPage extends StatefulWidget {
  @override
  _InputPageState createState() => _InputPageState();
}

class _InputPageState extends State<InputPage> {
  Gender gender;
  double height = 170.0;
  int weight = 60;
  int age = 20;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('BMI Calculator'),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Expanded(
            child: Row(
              children: [
                Expanded(
                  child: CustomCard(
                    color: gender == Gender.male
                        ? kCustomCardActiveColor
                        : kCustomCardInactiveColor,
                    child: GenderIcon(
                      iconGender: Gender.male,
                    ),
                    onTap: () {
                      setState(() {
                        gender = Gender.male;
                      });
                    },
                  ),
                ),
                Expanded(
                  child: CustomCard(
                    color: gender == Gender.female
                        ? kCustomCardActiveColor
                        : kCustomCardInactiveColor,
                    child: GenderIcon(
                      iconGender: Gender.female,
                    ),
                    onTap: () {
                      setState(() {
                        gender = Gender.female;
                      });
                    },
                  ),
                ),
              ],
            ),
          ),
          Expanded(
            child: CustomCard(
              color: kCustomCardInactiveColor,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Text(
                    'HEIGHT',
                    style: kCustomIconLabelTextStyle,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.baseline,
                    textBaseline: TextBaseline.alphabetic,
                    children: [
                      Text(
                        height.toStringAsFixed(0),
                        style: kCustomIconNumberTextStyle,
                      ),
                      Text(
                        'cm',
                        style: kCustomIconLabelTextStyle,
                      ),
                    ],
                  ),
                  Slider(
                    value: height,
                    min: 120.0,
                    max: 220.0,
                    onChanged: (value) {
                      setState(() {
                        height = value;
                      });
                    },
                    activeColor: Theme.of(context).accentColor,
                  )
                ],
              ),
            ),
          ),
          Expanded(
            child: Row(
              children: [
                Expanded(
                  child: CustomCard(
                    color: kCustomCardInactiveColor,
                    child: NumberSelector(
                      name: 'WEIGHT',
                      number: weight,
                      onIncreament: () {
                        setState(() {
                          weight++;
                        });
                      },
                      onDecrement: () {
                        setState(() {
                          weight--;
                        });
                      },
                    ),
                  ),
                ),
                Expanded(
                  child: CustomCard(
                    color: kCustomCardInactiveColor,
                    child: NumberSelector(
                        name: 'AGE',
                        number: age,
                        onIncreament: () {
                          setState(() {
                            age++;
                          });
                        },
                        onDecrement: () {
                          setState(() {
                            age--;
                          });
                        }),
                  ),
                ),
              ],
            ),
          ),
          BottomButton(
            onPressed: () {
              BMICalculator bmi = BMICalculator(
                height: height,
                weight: weight,
              );
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => Reslut(
                    bmi: bmi.calculate().toStringAsFixed(1),
                    message: bmi.message(),
                    status: bmi.status(),
                  ),
                ),
              );
            },
            text: 'CALCULATE',
          ),
        ],
      ),
    );
  }
}
