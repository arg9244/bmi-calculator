import 'package:bmi_calculator/src/logic/models/consts.dart';
import 'package:bmi_calculator/src/view/components/bottom_button.dart';
import 'package:bmi_calculator/src/view/components/custom_card.dart';
import 'package:flutter/material.dart';

class Reslut extends StatelessWidget {
  Reslut({
    @required this.bmi,
    @required this.message,
    @required this.status,
  });

  final String status;
  final String bmi;
  final String message;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('BMI Calculator'),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Padding(
            padding: EdgeInsets.all(15.0),
            child: Text(
              'Your Result',
              style: kBottomButtonTextStyle,
            ),
          ),
          Expanded(
            child: CustomCard(
              color: kCustomCardInactiveColor,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    status,
                    style: kCustomIconLabelTextStyle.copyWith(
                      color: Theme.of(context).accentColor,
                    ),
                  ),
                  Text(
                    bmi,
                    style: kCustomIconNumberTextStyle,
                  ),
                  Padding(
                    padding: const EdgeInsets.all(15.0),
                    child: Text(
                      message,
                      style: kCustomIconLabelTextStyle,
                      textAlign: TextAlign.center,
                    ),
                  ),
                ],
              ),
            ),
          ),
          SizedBox(
            height: 20.0,
          ),
          Container(
            width: double.infinity,
            child: BottomButton(
              onPressed: () {
                Navigator.pop(context);
              },
              text: 'RE-CALCULATE',
            ),
          )
        ],
      ),
    );
  }
}
