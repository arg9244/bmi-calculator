import 'package:bmi_calculator/src/view/screens/input.dart';
import 'package:flutter/material.dart';
import 'logic/models/consts.dart';

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'BMI Calculator',
      theme: ThemeData.dark().copyWith(
        primaryColor: kPrimaryColor,
        scaffoldBackgroundColor: kScaffoldBackgroundColor,
        accentColor: kAccentColor,
      ),
      home: InputPage(),

      //debug properties
      debugShowCheckedModeBanner: false,
    );
  }
}
