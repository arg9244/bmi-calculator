import 'package:flutter/material.dart';

//enums
enum Gender {
  male,
  female,
}

// Colors
const kPrimaryColor = Color(0xFF0A0E21);
const kScaffoldBackgroundColor = Color(0xFF0A0E21);
const kAccentColor = Color(0xFF3BCC90);
const kCustomCardActiveColor = Color(0xFF1D1E33);
const kCustomCardInactiveColor = Color(0xFF111328);
const kCustomIconBigTextColor = Color(0xFF8D8E98);

// Text Style
const kCustomIconLabelTextStyle = TextStyle(
  fontSize: 18.0,
  color: kCustomIconBigTextColor,
);

const kCustomIconNumberTextStyle = TextStyle(
  fontSize: 50.0,
  fontWeight: FontWeight.w900,
);

const kBottomButtonTextStyle = TextStyle(
  fontSize: 40.0,
  fontWeight: FontWeight.bold,
  color: Colors.white,
);
