import 'package:flutter/material.dart';

class BMICalculator {
  BMICalculator({
    @required this.height,
    @required this.weight,
  });

  final int weight;
  final double height;

  double _bmi;

  double calculate() => _bmi = weight / ((height / 100) * (height / 100));

  String status() {
    if (_bmi >= 25.0) {
      return 'OVERWEIGHT';
    } else if (_bmi > 18.5) {
      return 'NORMAL';
    } else {
      return 'UNDERWEIGHT';
    }
  }

  String message() {
    if (_bmi >= 25.0) {
      return 'You have a higher than normal body weight. Try to exercise more.';
    } else if (_bmi > 18.5) {
      return 'You have a noraml body weight. Good job!';
    } else {
      return 'You have a lower than normal body weight. Try to eat more.';
    }
  }
}
